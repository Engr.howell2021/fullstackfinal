import React from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Highlights(){
	return(
		<Row>
            <h1 className='text-center m-5'>The Latest</h1>
			<Col xs={12} md={3}>
			<Card style={{ width: '18rem' }}>
  <Card.Img className='mt-3' variant="top"  src={require("../images/electric.png")}  />
  <Card.Body className='mt-5'>
  <Card.Title>
							<h5>Men's Epic React Flyknit Running shoes</h5>
						</Card.Title>
	<Button variant="dark btn-block"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
  </Card.Body>
</Card>
			</Col>
					
			<Col xs={12} md={3}>
			<Card style={{ width: '18rem' }}>
  <Card.Img variant="top"  src={require("../images/light.png")}  />
  <Card.Body>
  <Card.Title>
							<h5>Men's Epic React Flyknit Running shoes</h5>
						</Card.Title>
	<Button variant="dark btn-block"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
  </Card.Body>
</Card>
			</Col>

			<Col xs={12} md={3}>
			<Card className='pt-5'  style={{ width: '18rem' }}>
  <Card.Img className='mt-3' variant="top"  src={require("../images/blue.png")}  />
  <Card.Body>
  <Card.Title>
							<h5 className='mt-5 pt-4'>Men's Epic React Flyknit Running shoes</h5>
						</Card.Title>
	<Button variant="dark btn-block"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
  </Card.Body>
</Card>
			</Col>

			<Col xs={12} md={3}>
			<Card className='pt-5 pb-2'  style={{ width: '18rem' }}>
  <Card.Img variant="top"  src={require("../images/white.png")}  />
  <Card.Body>
  <Card.Title>
							<h5>Men's Epic React Flyknite Running shoes</h5>
						</Card.Title>
	<Button variant="dark btn-block"><Link to="/products" className='nav-link text-white'>SHOP</Link></Button>
  </Card.Body>
</Card>
			</Col>
		</Row>
		);
}
